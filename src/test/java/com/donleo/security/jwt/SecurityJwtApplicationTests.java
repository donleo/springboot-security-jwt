package com.donleo.security.jwt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class SecurityJwtApplicationTests {

    @Test
    void TestSubString() {
        String s1 = "bearer kldwkakdjadwa";
        String HEADER = "Bearer";
        s1 = s1.substring(HEADER.length()).trim();
        System.out.println("截取后的字符串：" + s1);
    }

}
