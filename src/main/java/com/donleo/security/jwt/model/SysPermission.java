package com.donleo.security.jwt.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.Date;

/**
 * @author liangd
 * date 2020-12-08 18:28
 * code  权限表
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@TableName(value = "sys_permission")
public class SysPermission implements GrantedAuthority {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer pid;
    private String name;
    /**
     * 权限值
     */
    private String value;
    private String icon;
    private Integer type;
    private String uri;
    private Integer status;
    private Date createTime;
    private String sort;

    @Override
    public String getAuthority() {
        return this.value;
    }
}
