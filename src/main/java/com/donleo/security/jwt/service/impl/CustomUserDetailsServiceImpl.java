package com.donleo.security.jwt.service.impl;

import com.donleo.security.jwt.model.SysPermission;
import com.donleo.security.jwt.model.SysUser;
import com.donleo.security.jwt.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;

/**
 * @author liangd
 * date 2020-12-08 19:10
 * code 自定义UserDetailsService
 */
@Service("userDetailsService")
public class CustomUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    ISysUserService sysUserService;

    /**
     * 从数据库读取用户名认证
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user= sysUserService.getUserByName(username);
        List<SysPermission> permissionList= sysUserService.getPermissionsByUserId(user.getId());
        //获取用户拥有的权限
        HashSet<SysPermission> permissions = new HashSet<>(permissionList);
        user.setAuthorities(permissions);
        return user;
    }
}
