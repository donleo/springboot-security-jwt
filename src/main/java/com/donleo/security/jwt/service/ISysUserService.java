package com.donleo.security.jwt.service;

import com.donleo.security.jwt.model.SysPermission;
import com.donleo.security.jwt.model.SysUser;
import com.donleo.security.jwt.vo.LoginParams;

import java.util.List;

/**
 * @author liangd
 * date 2020-12-10 14:46
 * code
 */
public interface ISysUserService {
    /**
     * 根据用户名查询用户
     * @param name
     * @return
     */
    SysUser getUserByName(String name);

    /**
     * 根据用户Id查询用户权限
     * @param id
     * @return
     */
    List<SysPermission> getPermissionsByUserId(Integer id);

    /**
     * 用户登录
     * @param loginParams
     * @return
     */
    String login(LoginParams loginParams);

    /**
     * 用户注册
     * @param sysUser
     * @return
     */
    Boolean register(SysUser sysUser);
}
