package com.donleo.security.jwt.controller;

import com.donleo.security.jwt.common.CommonResult;
import com.donleo.security.jwt.mapper.ISysRoleMapper;
import com.donleo.security.jwt.model.SysRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author liangd
 * date 2020-12-10 15:00
 * code 系统角色控制层
 */
@RestController
@RequestMapping("/role")
public class SysRoleController {

    @Autowired
    private ISysRoleMapper sysRoleMapper;

    @PreAuthorize("hasAuthority('wx:product:readtest')")
    @GetMapping("/findById")
    public CommonResult findById(Integer id){
        SysRole sysRole = sysRoleMapper.selectById(id);
        return CommonResult.success(sysRole);
    }

    @PreAuthorize("hasAuthority('wx:product:read')")
    @GetMapping("/findAll")
    public CommonResult findAll(){
        List<SysRole> sysRoleList = sysRoleMapper.selectList(null);
        return CommonResult.success(sysRoleList);
    }
}
