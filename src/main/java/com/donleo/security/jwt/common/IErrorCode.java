package com.donleo.security.jwt.common;

/**
 * @author liangd
 * date 2020-12-02 18:28
 * code  错误码接口定义
 */
public interface IErrorCode {
    /**
     * 获取状态码
     */
    long getCode();

    /**
     * 获取提示信息
     */
    String getMessage();
}
