package com.donleo.security.jwt.vo;

import lombok.Data;

/**
 * @author liangd
 * date 2020-12-10 15:21
 * code 登录用户参数
 */
@Data
public class LoginParams {
    private String username;
    private String password;
}
