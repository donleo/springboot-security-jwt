package com.donleo.security.jwt.config;

import com.donleo.security.jwt.filter.JwtAuthenticationTokenFilter;
import com.donleo.security.jwt.service.impl.CustomUserDetailsServiceImpl;
import com.donleo.security.jwt.utils.RestAuthenticationEntryPoint;
import com.donleo.security.jwt.utils.RestfulAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author liangd
 * date 2020-12-10 14:43
 * <p>
 * .@EnableGlobalMethodSecurity 是否允许方法上设置权限
 * prePostEnabled 方法执行前/后 默认为false
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;

    @Autowired
    private RestfulAccessDeniedHandler restfulAccessDeniedHandler;

    /**
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.cors().and()
                .csrf().disable()
                // 基于token，所以不需要 securityContext
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                //都可以访问
                .antMatchers("/css/**", "/js/**", "/fonts/**", "/user/login", "/user/register").permitAll()
                .antMatchers(HttpMethod.OPTIONS).permitAll()
                //任何请求都需要认证
                .anyRequest().authenticated()
                .and()
                .userDetailsService(userDetailsService)
        ;
        //自定义 登录界面
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);

        //添加自定义未授权和未登录结果返回
        http.exceptionHandling()
                .accessDeniedHandler(restfulAccessDeniedHandler)
                .authenticationEntryPoint(restAuthenticationEntryPoint);
    }

   /* @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //设置哪些路径可以访问
                .antMatchers("/css/**", "/js/**", "/fonts/**").permitAll()
                //需要相应的角色才能访问
                .antMatchers("/users/**").hasRole("ADMIN")
                .anyRequest().authenticated()   // 任何请求都需要认证
                .and()
                .formLogin() //基于Form表单登录验证
                .and()
                .userDetailsService(userDetailsService);
    }*/

    /**
     * 创建passwordEncoder对象
     *
     * @return
     */
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}