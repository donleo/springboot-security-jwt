package com.donleo.security.jwt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @author liangd
 * date 2020-12-10 16:00
 * code 跨域配置
 * .@Order 注解用来声明组件的顺序，值越小，优先级越高，越先被执行/初始化。如果没有该注解，则优先级最低。
 */
@Configuration
@Order(1)
public class FilterConfig {
    private CorsConfiguration buildConfig(){
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        // 允许任何的head头部
        corsConfiguration.addAllowedHeader("*");
        // 允许任何域名使用
        corsConfiguration.addAllowedOrigin("*");
        // 允许任何的请求方法
        corsConfiguration.addAllowedMethod("*");
        corsConfiguration.setAllowCredentials(true);
        return corsConfiguration;
    }

    /**
     * 添加CorsFilter拦截器，对任意的请求使用
     * @return CorsFilter
     */
    @Bean
    public CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", buildConfig());
        return new CorsFilter(source);
    }
    /*
        配置security-jwt步骤总结：
        1、引入pom依赖
        2、配置yml
        3、新建vo登录参数          LoginParams
        4、创建jwt工具类          JwtTokenUtil
        5、配置Jwt登录授权过滤器   JwtAuthenticationTokenFilter
        6、实现登录生成token逻辑   SysUserController

        7、跨域配置               FilterConfig
        8、配置当未登录或者token失效访问接口时，自定义的返回结果 RestAuthenticationEntryPoint
        9、配置访问接口没有权限时，自定义的返回结果             RestfulAccessDeniedHandler

     */
}
