package com.donleo.security.jwt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donleo.security.jwt.model.SysRole;

/**
 * @author liangd
 * date 2020-12-10 15:01
 * code
 */
public interface ISysRoleMapper extends BaseMapper<SysRole> {

}
