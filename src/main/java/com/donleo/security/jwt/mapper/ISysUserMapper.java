package com.donleo.security.jwt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.donleo.security.jwt.model.SysPermission;
import com.donleo.security.jwt.model.SysUser;

import java.util.List;

/**
 * @author liangd
 * date 2020-12-10 14:37
 * code
 */
public interface ISysUserMapper extends BaseMapper<SysUser> {

    List<SysUser> getUserByName(String name);

    List<SysPermission> getPermissionsByUserId(Integer id);
}
